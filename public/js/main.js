var SDKHandler = {
  onLoad: function() {
    SDKHandler.init();
    SDKHandler.setToggleButton();

    NikiSDK.showChat();

    NikiSDK.onIntent(function(payload) {
      // your code here

      NikiSDK.onIntentComplete(payload);
    });
  },

  init: function() {
    NikiSDK.init({
      merchant: {
        id: '1529385291',
        key: 'MyLXJ986Ha66Bzl0BKhTyZDlc3FRN9Fn/CNjhnGy67=',
        domain: 'tatacapital'
      }

    });
  },

  setToggleButton: function() {
    var toggleButton = document.getElementById('toggle-niki');
    SDKHandler.setClickListener(toggleButton, function() {
      NikiSDK.toggleChat();
    });
  },

  setClickListener: function(element, handler) {
    if (element.addEventListener) {
      element.addEventListener('click', handler, false);
    } else if (element.attachEvent) {
      element.attachEvent('onclick', handler);
    }
  }
};
