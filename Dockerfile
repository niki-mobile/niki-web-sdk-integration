FROM node:10
COPY . .
RUN yarn install
EXPOSE 3000
CMD ["yarn","start"]
